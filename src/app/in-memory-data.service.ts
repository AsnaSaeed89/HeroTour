import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 1, name: 'Spike' },
      { id: 2, name: 'Bombshell' },
      { id: 3, name: 'Fantastic' },
      { id: 4, name: 'Mr.Elastic' },
      { id: 5, name: 'Mr.Magnetic' },
      { id: 6, name: 'Mr.Rubber Man' },
      { id: 7, name: 'Miss.Fantasy' },
      { id: 8, name: 'Lava' },
      { id: 9, name: 'Hurricane' },
      { id: 10, name: 'Dr.Shock' }
    ];
    return {heroes};
  }
}
